const express = require('express')
const https = require('https')

const app = express()

const rootUrl = 'https://gitlab.com'
const basePath = '/winniehell/water-child/merge_requests'
const baseUrl = rootUrl + basePath

const diffAdditionPattern = /^\+([^+]*)$/
const linkPattern = new RegExp(`<a href="(${basePath}/\\d+)">`)
const thumbsPattern = /<i aria-hidden="true" data-hidden="true" class="fa fa-thumbs-(down|up)"><\/i>\s*(\d+)/g

const cache = {}

function cachedRequest(url) {
  const now = new Date().getTime()
  const cachedResponse = cache[url]

  if (cachedResponse && (cachedResponse.expires > now)) {
    return Promise.resolve(cachedResponse.data)
  }

  return new Promise((resolve, reject) => {
    const request = https.request(url, (response) => {
      const receivedData = []

      response.on('data', (data) => {
        receivedData.push(data)
      })

      response.on('end', () => {
        const data = receivedData.join('')
        cache[url] = {
          data,
          expires: now + 60 * 60 * 1000
        }
        resolve(data)
      })
    })

    request.on('error', reject)
    request.end()
  })
}

function parseMergeRequestUrl(html) {
  const match = linkPattern.exec(html)

  if (match) {
    return rootUrl + match[1]
  } else {
    return null
  }
}

function parseScore(html) {
  let score = 0

  let match
  while(match = thumbsPattern.exec(html)) {
    const value = parseInt(match[2])
    if (match[1] === 'up') {
      score += value
    } else if (match[1] === 'up') {
      score -= value
    }
  }

  return score
}

function fetchMergeRequests() {
  return cachedRequest(`${baseUrl}.json`)
    .then((data) => {
      try {
        return JSON.parse(data)
      } catch(e) {
        throw new Error(`Unable to parse JSON: ${data}`)
      }
    })
    .then(parsedResponse => parsedResponse.html
      .split('<li class="merge-request"')
      .filter(mergeRequestHtml => mergeRequestHtml.indexOf('<a class="author_link  " href="/winniehell">') > -1)
      .map((mergeRequestHtml) => {
        return {
          url: parseMergeRequestUrl(mergeRequestHtml),
          score: parseScore(mergeRequestHtml)
        }
      })
      .filter(mergeRequest => mergeRequest.url !== null)
    )
}

function parseDiff(diff) {
  return diff
    .split('\n')
    .map((line) => {
      const match = diffAdditionPattern.exec(line)
      if (match) {
        return match[1]
      } else {
        return null
      }
    })
    .filter(line => line !== null)
    .join('\n')
}

function fetchDiff(mergeRequest) {
  return cachedRequest(`${mergeRequest.url}.diff`)
    .then((data) => {
      mergeRequest.text = parseDiff(data.toString())
      return mergeRequest
    })
}

app.get('/', function (req, res) {
  res.redirect('https://winniehell.de/water-child/')
})

app.get('/merge-requests', function (req, res) {
  fetchMergeRequests()
    .then(mergeRequests => Promise.all(mergeRequests.map(fetchDiff)))
    .then((mergeRequests) => {
      res.set('Access-Control-Allow-Origin', '*')
      res.json(mergeRequests)
    })
    .catch(console.error)
})

const port = parseInt(process.env.PORT) || 3000
app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
